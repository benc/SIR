#!/usr/bin/env/python3
from math import *
from argparse import *
from pdb import set_trace as brk


class SIR:
	def __init__(self, beta, infectious_for, death_delay, ifr, pop_size):
		"""
		beta is the infection rate (in new infections per infectious
		susceptible per day).

		infectious_for is the number of days you are infectious for after
		getting infected.

		death_delay is how long it takes to die after getting infected.

		ifr is Infection Fatality Ratio-- the number of infected who go on to
		die.
		"""
		self.pop_size = pop_size
		self.beta = beta

		self.ifr = ifr

		self.death_delay = death_delay
		self.infectious_for = infectious_for

		self.susceptible = pop_size
		self.infected = 1
		self.recovered = 0
		self.dead = 0
		self.immune = 0

		# This is the number of new dead and new infected per day
		self.new_dead = 0
		self.new_infected = 0
		# This is yesterday's new dead count
		self.prev_new_dead = 0
		self.prev_log_dead = 0

		# Useful things to plot, with optional functions where they aren't just
		# attributes
		self.charts = (
				("dead", "lines"),
				("new_dead", "boxes"),
				("new_infected", "boxes"),
				("infected", "lines"),
				("recovered", "lines"),
				("susceptible_ratio", "lines", self.susceptible_ratio),
				("infected_ratio", "lines", self.infected_ratio),
				("log_dead", "lines", self.log_dead),
				("log_dead_gradient", "lines", self.log_dead_gradient),
				("dead_vs_immune", "lines", self.dead_vs_immune),
				("new_dead_growth", "lines", self.new_dead_growth),
				)

	def log_dead(self):
		if self.dead:
			return max(0, log(self.dead, 10))
		else:
			return 0

	def log_dead_gradient(self):
		ld = self.log_dead()

		if self.prev_log_dead:
			ret = ld - self.prev_log_dead
		else:
			ret = 0

		self.prev_log_dead = ld
		return ret

	def infected_ratio(self):
		"""The proportion of antigen tests you would expect to be positive"""
		return self.infected / self.pop_size

	def susceptible_ratio(self):
		"""The proportion of antigen tests you would expect to be positive"""
		return self.susceptible / self.pop_size

	def dead_vs_immune(self):
		immune_ratio = self.immune / self.pop_size
		return " ".join([str(s) for s in (self.dead, immune_ratio)])

	def new_dead_growth(self):
		if not self.prev_new_dead: return 0
		return self.new_dead / self.prev_new_dead

	def step(self, h):
		beta = self.beta
		s, i, r = self.susceptible, self.infected, self.recovered

		# This depends on the S because those are the people catching it, and
		# also on I because those are the guys they're catching it from.
		new_infections = beta * s * i * h

		# Now of those new_infections, some will die and some will recover,
		# given by IFR. The grim reaper decides that now.
		doomed = new_infections * self.ifr
		gonna_make_it = new_infections - doomed

		new_deaths = self.dying.pop(0)
		self.dying.append(doomed)

		new_recoveries = self.recovering.pop(0)
		self.recovering.append(gonna_make_it)

		self.susceptible = max(0, self.susceptible - new_infections)
		self.infected += new_infections - new_recoveries - new_deaths
		self.recovered += new_recoveries - new_deaths
		self.dead += new_deaths
		self.immune = self.recovered + self.dead

		# Track these so they can also get plotted
		self.prev_new_dead = self.new_dead
		self.new_dead = new_deaths
		self.new_infected = new_infections

	def output(self):
		with open("plot.gpi", "w") as fp:
			print("""
set term png
set grid""", file=fp)
			for c in self.charts:
				fname = c[0]
				style = c[1]

				print('''
set output "{fname}.png"
plot "{fname}" with {style}'''.format(fname=fname, style=style), file=fp)

		print("Now run gnuplot plot.gpi")

		with open("show.sh", "w") as fp:
			print("gnuplot plot.gpi", file=fp)
			for c in self.charts:
				print("display {}.png &".format(c[0]), file=fp)

		print("Run bash show.sh to display all graphs")

	def simulate(self, num_days, inv_h=1):
		charts = {}

		def attr_fn(name):
			return lambda: getattr(self, name)

		for c in self.charts:
			name = c[0]
			if len(c) == 3:
				charts[name] = c[2]
			else:
				charts[name] = attr_fn(name)

		fps = {c: open(c, "w") for c in charts.keys()}

		self.dying = [0] * int(self.death_delay * inv_h)
		self.recovering = [0] * int(self.infectious_for * inv_h)

		milestone = 0
		for _ in range(num_days):
			for _ in range(inv_h):
				self.step(1.0/inv_h)

			for c, fn in charts.items():
				fps[c].write("{}\n".format(fn()))

			if self.dead > pow(10, milestone):
				print("{} ({:.1f}%) immune at {} dead".format(
					int(self.immune),
					self.immune * 100 / self.pop_size,
					int(self.dead)))
				milestone += 1

		for k, v in fps.items():
			print("Wrote {}".format(k))
			v.close()

		self.output()


def main():
	ap = ArgumentParser()

	ap.add_argument('-i', "--ifr", default=0.1, type=float,
			help="IFR as a percentage")
	ap.add_argument('-p', "--population", default=60, type=float,
			help="population in millions")
	ap.add_argument('-d', "--days", default=250, type=int,
			help="number of days to simulate")
	ap.add_argument('-j', "--daily-increase", default=1.33, type=float,
			help="Number that new cases multiply early in the outbreak")
	ap.add_argument('-f', "--infectious_for", default=4.5, type=float,
			help="Number of days someone is infectious for")
	ap.add_argument('-t', "--death_delay", default=20, type=int,
			help="Number of days from infection it takes to die")
	ap.add_argument('-s', "--timestep", default=2,
			dest="inv_h", type=int, help="Inverse of Timestep")

	args = ap.parse_args()

	# Estimate beta from daily increase and population size.
	pop = args.population * 1e6
	beta = (args.daily_increase - 1) / pop

	sir = SIR(beta, args.infectious_for, args.death_delay, args.ifr/100, pop)
	sir.simulate(args.days, args.inv_h)

	print("Total dead: {} or {}%".format(sir.dead, sir.dead * 100/pop))

	total = sir.infected + sir.recovered + sir.dead
	total_ratio = total / pop
	r0 = 1 / (1 - total_ratio)
	print("Total ever infected: {:.2f}% implies R0 of {}".format(
		total_ratio * 100, r0))


if __name__ == "__main__":
	main()
