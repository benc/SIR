This is a SIR model with default parameters set to best guesses for the 2020
Covid-19 outbreak, that can be used to estimate roughly where a country might
be on its way to herd immunity based on reported numbers of deaths and daily
antigen tests (where available). At the time of writing nobody has done any
antibody tests.

It differs a bit from a regular SIR model in that we don't use 'gamma' for a
recovery rate. Instead recovery and death are modelled as delay buffers-- after
being infected you go into one or other of those buffers and join the
"recovered" or "dead" compartments some constant number of days later.

It outputs data files of various interesting things and a gnuplot and bash
script to plot and display them.
